package bit_counting

import (
	"strconv"
	"strings"

)

func CountBits(x uint) int {

	n := strconv.FormatInt(int64(x), 2)
	return int (strings.Count(n, "1"))
}